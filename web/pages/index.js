import Head from 'next/head';
import styles from '../styles/Home.module.css';
import classnames from 'classnames';

//components
import Header from 'components/Home/Header';
import Section from 'components/Home/Section';
import Projects from 'components/Home/Projects';
import Banner from 'components/Home/Banner';
import Contacts from 'components/Home/Contacts';

import data from 'data/data.json';
import examplesData from 'data/examples.json';

export default function Home() {
  return (
    <div className={classnames('border-t-4 border-blue-600', styles.container)}>
      <Head>
        <title>Smart Contracts Projects - Tezos Israel</title>
        <link rel="icon" href="/logo_blue.svg" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1"
        />
        <meta
          name="description"
          content="Learn Tezos smart contract syntax and functionality in a range of coding languages."
        />
      </Head>

      <Header
        title={data.header.title}
        description={data.header.desc}
        buttonLink={data.header.button.link}
        buttonLabel={data.header.button.label}
      />

      <Section title={data.projects.title}>
        <Projects projects={examplesData} />
      </Section>

      <Banner banner={data.banner} />

      <Section title={data.contact.title}>
        <Contacts contactsList={data.contact.list} />
      </Section>
    </div>
  );
}
