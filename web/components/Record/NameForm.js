import PropTypes from 'prop-types';
import React, { useState } from 'react';
import Loader from 'react-loader-spinner';
import SectionComponent from './SectionComponent.js';
export default function NameForm({
  isLoading,
  submitName,
  reloadStorage,
  name,
}) {
  const [inputName, setName] = useState(name);

  return (
    <SectionComponent icon="/form_icon.svg" title="Change Name">
      <div className="flex">
        <div className="w-1/3">
          <label className="text-gray-500 font-bold text-sm mr-3 block mb-3">
            Name:
          </label>
          <input
            value={inputName}
            onChange={(e) => setName(e.target.value)}
            placeholder="Name"
            className="border border-gray-300 bg-gray-100 rounded-md px-2 py-2 w-full"
          />
        </div>
        <div className="ml-3 mt-8 w-1/3">
          {isLoading ? (
            <div className="pt-2 flex">
              <Loader
                type="TailSpin"
                color="#cacaca"
                height={25}
                width={25}
                className="mr-3"
              />
              Updating ...
            </div>
          ) : (
            <>
              <button
                className="bg-blue-600 text-white rounded-md px-10 py-2 w-full"
                onClick={async () => {
                  cleanInputs();
                  await submitName(inputName);
                  await reloadStorage();
                }}
              >
                Save name
              </button>
            </>
          )}
        </div>
      </div>
    </SectionComponent>
  );
  async function cleanInputs() {
    setName('');
  }
}

NameForm.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  reloadStorage: PropTypes.func.isRequired,
  submitName: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
};
