import PropTypes from 'prop-types';
function ErrorComponent({ contractError, operationError }) {
  return (
    <div className="wallet-info">
      {contractError && <div>Contract Error: {contractError}</div>}
      {operationError && <div>Operation Error: {operationError}</div>}
    </div>
  );
}

ErrorComponent.propTypes = {
  contractError: PropTypes.string,
  operationError: PropTypes.string,
};
export default ErrorComponent;
