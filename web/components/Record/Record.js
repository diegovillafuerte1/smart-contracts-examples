import PropTypes from 'prop-types';
import { useEffect } from 'react';

import { Tezos } from 'hooks/use-beacon';
import classnames from 'classnames';

import Layout from 'components/Layout';

import FullForm from './FullForm.js';
import NameForm from './NameForm.js';
import AgeForm from './AgeForm.js';
import StorageField from './StorageField.js';
import ErrorComponent from './ErrorComponent.js';

import { useFunction } from './use-functions.js';
import { useContract } from './use-contract';

import styles from './Record.module.css';

export default function Record({ contractAddress, userAddress }) {
  const {
    name,
    age,
    error: contractError,
    loading,
    contract,
    connect,
    reloadStorage,
  } = useContract(Tezos, contractAddress);

  useEffect(() => {
    connect(contractAddress);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [contractAddress]);

  const {
    operationLoading,
    operationError,
    submitAll,
    submitAge,
    submitName,
  } = useFunction(contract);

  return (
    <Layout
      userAddress={userAddress}
      contractAddress={contractAddress}
      title="Record"
    >
      <div
        className={classnames(
          `w-full min-h-full box-border py-10`,
          styles.container
        )}
      >
        <div className="max-w-4xl mx-auto">
          <StorageField
            isLoading={loading || operationLoading}
            name={name}
            age={age}
          />
          <FullForm
            isLoading={loading || operationLoading}
            submitAll={submitAll}
            reloadStorage={reloadStorage}
            name={name}
            age={age}
          />
          <NameForm
            isLoading={loading || operationLoading}
            submitName={submitName}
            reloadStorage={reloadStorage}
            name={name}
          />
          <AgeForm
            isLoading={loading || operationLoading}
            submitAge={submitAge}
            reloadStorage={reloadStorage}
            age={age}
          />
        </div>
        <ErrorComponent
          contractError={contractError}
          operationError={operationError}
        />
      </div>
    </Layout>
  );
}

Record.propTypes = {
  contractAddress: PropTypes.string.isRequired,
  userAddress: PropTypes.string.isRequired,
};
