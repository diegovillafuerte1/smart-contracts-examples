import { useState } from 'react';

export function useFunction(contract) {
  const [operationLoading, setOperationLoading] = useState(false);
  const [operationError, setOperationError] = useState('');

  return {
    submitAll,
    submitAge,
    submitName,
    operationError,
    operationLoading,
  };

  async function submitAll(name, age) {
    if (!contract) {
      return;
    }
    setOperationError('');
    try {
      setOperationLoading(true);
      const op = await contract.methods.setAll(name, Number(age)).send();
      await op.confirmation();
    } catch (error) {
      setOperationError(error.message);
    } finally {
      setOperationLoading(false);
    }
  }

  async function submitName(name) {
    if (!contract) {
      return;
    }
    setOperationError('');
    try {
      setOperationLoading(true);
      const op = await contract.methods.setName(name).send();
      await op.confirmation();
      setOperationLoading(false);
    } catch (error) {
      setOperationError(error.message);
    } finally {
      setOperationLoading(false);
    }
  }

  async function submitAge(age) {
    if (!contract) {
      return;
    }
    setOperationError('');
    try {
      setOperationLoading(true);
      const op = await contract.methods.setAge(Number(age)).send();
      await op.confirmation();
      setOperationLoading(false);
    } catch (error) {
      setOperationError(error.message);
    } finally {
      setOperationLoading(false);
    }
  }
}
