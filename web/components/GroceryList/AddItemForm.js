import PropTypes from 'prop-types';
import { useState } from 'react';

export default function AddItemForm({ error, adding, onSubmit, setError }) {
  const [name, setName] = useState('');
  const [amount, setAmount] = useState('');

  return (
    <div className="container mx-auto px-10 py-10">
      <div className="w-2/3 mx-auto">
        <div
          className={`add-item-form mb-8 flex px-5 py-5 rounded-md	justify-between border`}
        >
          <div className="input-field w-full mr-2">
            <input
              type="text"
              className="border py-2 px-2 rounded-md  w-full"
              value={name}
              onChange={(e) => {
                setName(e.target.value);
                setError(false);
              }}
              placeholder="Name"
            />
          </div>
          <div className="input-field w-full mr-2">
            <input
              type="number"
              className="border py-2 px-2 rounded-md  w-full"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
              placeholder="Amount"
            />
          </div>
          <div className="button-field">
            <button
              className={`btn bg-green-500 rounded-md px-4 py-2 text-white ${
                adding ? 'disabled' : ''
              }`}
              onClick={async () => {
                await onSubmit(name, amount);
                cleanInputs();
              }}
              disabled={adding}
            >
              {adding ? 'Adding...' : 'Add'}
            </button>
          </div>
        </div>

        {error && (
          <div className="flex px-8 py-3 mb-6 rounded-md	justify-between  bg-red-100 text-red-600">
            {error}
          </div>
        )}
      </div>
    </div>
  );

  function cleanInputs() {
    setName('');
    setAmount('');
  }
}

AddItemForm.propTypes = {
  error: PropTypes.string,
  adding: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  setError: PropTypes.func.isRequired,
};
