import PropTypes from 'prop-types';
import { Tezos } from 'hooks/use-beacon';
import { useState, useCallback, useEffect } from 'react';
import Layout from 'components/Layout';

import ItemsList from './ItemsList';
import AddItemForm from './AddItemForm';

export default function GroceryList({ contractAddress, userAddress }) {
  const [itemsList, setItemsList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [adding, setAdding] = useState(false);

  const getStorage = useCallback(
    async function getStorage() {
      setIsLoading(true);
      try {
        setItemsList([]);
        const contract = await Tezos.wallet.at(contractAddress); //should replace with Tezos.wallet
        const storage = await contract.storage();
        setItemsList(
          Array.from(storage.entries()).map(([name, amount]) => ({
            name,
            amount,
          }))
        );
      } catch (error) {
        setError(error.message);
      }

      setIsLoading(false);
    },
    [contractAddress]
  );

  useEffect(() => {
    getStorage();
  }, [getStorage]);

  return (
    <Layout
      userAddress={userAddress}
      contractAddress={contractAddress}
      title="Grocery List"
    >
      <AddItemForm
        contractAddress={contractAddress}
        itemsList={itemsList}
        getStorage={getStorage}
        error={error}
        setError={setError}
        adding={adding}
        onSubmit={onSubmit}
      />
      <ItemsList
        itemsList={itemsList}
        deleteItem={deleteItem}
        updateItem={updateItem}
        isLoading={isLoading}
        setItemsList={setItemsList}
      />
    </Layout>
  );

  async function onSubmit(name, amount) {
    if (!name && !amount) {
      return;
    }

    const hasItem = itemsList.some(
      (item) => item.name.toLowerCase() === name.toLowerCase()
    );

    setError(null);

    if (hasItem) {
      setError('Name ' + name + ' already exists.');
      return;
    }

    setAdding(true);

    try {
      const contractInstance = await Tezos.wallet.at(contractAddress);
      const operation = await contractInstance.methods
        .createItem(name, amount)
        .send();
      await operation.confirmation();
      getStorage();
    } catch (error) {
      setError(error.message);
    }
    setAdding(false);
  }

  async function deleteItem(item) {
    setIsLoading(true);
    try {
      const contractInstance = await Tezos.wallet.at(contractAddress);
      const operation = await contractInstance.methods.removeItem(item).send();
      await operation.confirmation();
      getStorage();
    } catch (error) {
      setError(error.message);
    }
    setIsLoading(false);
  }

  async function updateItem(item) {
    if (!item.newAmount) {
      return;
    }
    setIsLoading(true);
    try {
      const contractInstance = await Tezos.wallet.at(contractAddress);
      const operation = await contractInstance.methods
        .changeAmount(item.name, item.newAmount)
        .send();
      await operation.confirmation();
      getStorage();
    } catch (error) {
      setError(error.message);
    }

    setIsLoading(false);
  }
}

GroceryList.propTypes = {
  contractAddress: PropTypes.string.isRequired,
  userAddress: PropTypes.string.isRequired,
};
