import dynamic from 'next/dynamic';

const examples = {
  calculator: dynamic(() => import('./Calculator')),
  record: dynamic(() => import('./Record')),
  'grocery-list': dynamic(() => import('./GroceryList')),
  'hello-world': dynamic(() => import('./HelloWorld')),
};

export default examples;
