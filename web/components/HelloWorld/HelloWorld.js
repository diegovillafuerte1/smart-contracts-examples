import PropTypes from 'prop-types';
import Head from 'next/head';
import styles from './HelloWorld.module.css';
import useBeacon, { Tezos } from 'hooks/use-beacon';
import { useState, useCallback, useEffect } from 'react';
import Loader from 'react-loader-spinner';
import Layout from 'components/Layout';

export default function HelloWorld({ contractAddress, userAddress }) {
  const { connect } = useBeacon();
  const [message, setMessage] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const fetcher = useCallback(
    async () => await (await Tezos.contract.at(contractAddress)).storage(),
    [Tezos.contract, contractAddress]
  );

  useEffect(() => {
    connect();
    getStorage();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [contractAddress]);

  return (
    <Layout
      userAddress={userAddress}
      contractAddress={contractAddress}
      title="Hello World"
    >
      <div className={styles.container}>
        {!isLoading ? (
          <main className={styles.main}>
            <h1 className="font-bold text-6xl">
              Welcome to <span className="text-blue-600">Hello World!</span>
            </h1>

            <p className="my-10">
              <input
                type="text"
                className="border border-gray-400 px-3 py-1 rounded-md text-md"
                defaultValue={message}
                onChange={(e) => setMessage(e.target.value)}
              />
              <button
                className="bg-blue-600 px-3 py-1 rounded-md text-white text-md ml-3"
                onClick={updateItem}
              >
                Change Message
              </button>
            </p>

            {error && (
              <div>
                <code>{error}</code>
              </div>
            )}
          </main>
        ) : (
          <div className="text-center flex flex-col justify-center items-center fixed top-0 bottom-0 left-0 right-0 bg-gray-100 bg-opacity-25	">
            <Loader
              type="TailSpin"
              color="#cacaca"
              height={50}
              width={50}
              className="mb-3"
            />
            Fetching Storage
          </div>
        )}
      </div>
    </Layout>
  );

  async function updateItem() {
    setIsLoading(true);
    try {
      const contractInstance = await Tezos.wallet.at(contractAddress);
      const operation = await contractInstance.methods.default(message).send();
      await operation.confirmation();
      getStorage();
    } catch (error) {
      setError(error.message);
    }

    await getStorage();
    setIsLoading(false);
  }

  async function getStorage() {
    setIsLoading(true);
    try {
      const storage = await fetcher();
      setMessage(storage);
    } catch (error) {
      setError(error.message);
    }

    setIsLoading(false);
  }
}

HelloWorld.propTypes = {
  contractAddress: PropTypes.string.isRequired,
  userAddress: PropTypes.string.isRequired,
};
