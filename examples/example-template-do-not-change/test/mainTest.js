//change all occurances of 'Grocery' to fit contract names

const assert = require('assert');

//uncomment lang when proper contract exists / leave langs that you don't want to test commented
[
  /*"Caml", "Pascal", "Reason"*/
].forEach((lang) => {
  const contractName = 'Grocery' + lang;
  const Grocery = artifacts.require(contractName);

  contract(contractName, async function () {
    let instance = null;

    before(async () => {
      instance = await Grocery.deployed();
    });

    // First test - check if the storage was deployed correctly

    it('Should check the initial storage', async function () {
      const storage = await instance.storage();
      assert.strictEqual(storage.size, 0);
    });
  });
});
