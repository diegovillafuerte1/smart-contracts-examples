type name is string
type amt is nat
type storage is map(name, amt)
type return is list(operation) * storage
type action is
    | CreateItem of name * amt
    | RemoveItem of name
    | ChangeAmount of name * amt

function createItem(const name : name; const amt : amt; const store : storage) : return is
    case Map.find_opt(name, store) of
    | Some(item) -> ((nil : list(operation)), (failwith("ItemAlreadyExists") : storage))
    | None -> ((nil : list(operation)), Map.add(name, amt, store))
    end;

function removeItem(const name : name; const store : storage) : return is
    case Map.find_opt(name, store) of
    | None -> ((nil : list(operation)), (failwith("NoSuchItem") : storage))
    | Some(item) -> ((nil : list(operation)), Map.remove(name, store))
    end;

function changeAmount(const name : name; const amt : amt; const store : storage) : return is
    case Map.find_opt(name, store) of
    | None -> ((nil : list(operation)), (failwith("NoSuchItem") : storage))
    | Some(item) -> ((nil : list(operation)), Map.update(name, Some(amt), store))
    end;

function main(const action : action; const store : storage) : return is
    case action of
        | CreateItem(v) -> (createItem(v.0, v.1, store))
        | RemoveItem(v) -> (removeItem(v, store))
        | ChangeAmount(v) -> (changeAmount(v.0, v.1, store))
    end;