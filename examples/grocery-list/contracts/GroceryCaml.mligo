type name = string
type amt = nat
type storage = (name, amt) map
type return = (operation list) * storage
type action =
   | CreateItem of name * amt
   | RemoveItem of name
   | ChangeAmount of name * amt

let createItem(name, amt, store : name * amt * storage) = 
   match Map.find_opt name store with
   | Some (item) -> (failwith("ItemAlreadyExists") : storage)
   | None -> Map.add name amt store

let removeItem(name, store : name * storage) = 
   match Map.find_opt name store with
   | None -> (failwith("NoSuchItem") : storage)
   | Some (item) -> Map.remove name store

let changeAmount(name, amt, store : name * amt * storage) = 
   match Map.find_opt name store with
   | None -> (failwith("NoSuchItem") : storage)
   | Some (item) -> Map.update name (Some(amt)) store

let main (action, store : action * storage) : return =
   let store = match action with
   | CreateItem v -> (createItem(v.0, v.1, store) : storage)
   | RemoveItem v -> (removeItem(v, store) : storage)
   | ChangeAmount v -> (changeAmount(v.0, v.1, store) : storage)
in ([] : operation list), store
