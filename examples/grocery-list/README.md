# Grocery List:

## A smart contract example that represents a shopping list

---

#### Source Code:

- **Caml:** [GroceryCaml](https://gitlab.com/tezosisrael/smart-contracts-examples/-/blob/master/examples/grocery-list/contracts/GroceryCaml.mligo)
- **Pascal:** [GroceryPascal](https://gitlab.com/tezosisrael/smart-contracts-examples/-/blob/master/examples/grocery-list/contracts/GroceryPascal.ligo)
- **Reason:** [GroceryReason](https://gitlab.com/tezosisrael/smart-contracts-examples/-/blob/master/examples/grocery-list/contracts/GroceryReason.religo)

#### Contract addresses (florencenet):

- **Caml:** [KT1MewfbLcRLsXoWyAtsuj14deifnGxxKrqm](https://better-call.dev/florencenet/KT1MewfbLcRLsXoWyAtsuj14deifnGxxKrqm/operations)
- **Pascal:** [KT19LYERyx6vT3vPHLos6J239mXWkq9L29FR](https://better-call.dev/florencenet/KT19LYERyx6vT3vPHLos6J239mXWkq9L29FR/operations)
- **Reason:** [KT1GaEsnZZCvnLRCVrQ7x8T4HmbyXDHmH6x4](https://better-call.dev/florencenet/KT1GaEsnZZCvnLRCVrQ7x8T4HmbyXDHmH6x4/operations)

---

### API:

#### Entry-Points:

- `createItem(name, amount : string * nat)`
- `removeItem(name: string)`
- `changeAmount(name, amount : string * nat)`

#### Storage:

`(string, int) map`

#### Errors:

##### While In Progress:

- `NotDefined`: main entry-point gets the command to call an entry-point, but the entry-point doesn't exist yet.
- `NotImplemented`: the entry-point is defined, but not functional yet.

##### Operational Errors:

- `NoSuchItem`: if trying to access a non existing item (removeItem, ChangeAmount).
- `ItemAlreadyExists`: if trying to create an already existing item (createItem).

---

#### Contributors:

- **Caml:** Aharon Lando [@ahla85](https://gitlab.com/ahla85)

- **Pascal:** Aharon Lando [@ahla85](https://gitlab.com/ahla85)

- **Reason:** Adam Shinder [@adamshinder](https://gitlab.com/adamshinder)

- **DApp:** Ameed Jadallah [@ameedjadallah](https://gitlab.com/ameedjadallah)
