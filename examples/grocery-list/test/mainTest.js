const assert = require('assert');
['Caml', 'Pascal', 'Reason'].forEach((lang) => {
  const contractName = 'Grocery' + lang;
  const Grocery = artifacts.require(contractName);

  contract(contractName, async function () {
    let instance = null;

    before(async () => {
      instance = await Grocery.deployed();
    });

    // First test - check if the storage was deployed correctly

    it('Should check the initial storage', async function () {
      const storage = await instance.storage();
      assert.strictEqual(storage.size, 0);
    });

    // Failure test #1 - removeItem with a not existing key

    it("Should try to remove a not existing item and fail with 'NoSuchItem' message", async function () {
      try {
        await instance.removeItem('Item');
      } catch (error) {
        assert.strictEqual(error.message, 'NoSuchItem');
      }
    });

    // Failure test #2 - changeAmount with a not existing key

    it("Should try to change amount for a not existing item and fail with 'NoSuchItem' message", async function () {
      try {
        await instance.changeAmount('Item', 1);
      } catch (error) {
        assert.strictEqual(error.message, 'NoSuchItem');
      }
    });

    // Failure test #3 - createItem with an already existing key

    it("Should try to create an already existing item and fail with 'ItemAlreadyExists' message", async function () {
      await instance.createItem('Item', 1);
      try {
        await instance.createItem('Item', 1);
      } catch (error) {
        assert.strictEqual(error.message, 'ItemAlreadyExists');
      }
    });

    // Success test #1 - removeItem with valid value

    it('Should remove an existing item', async function () {
      await instance.removeItem('Item');
      const storage = await instance.storage();
      const item = await storage.get('Item');
      assert.strictEqual(item, undefined);
    });

    // Success test #2 - creatiItem with valid value

    it('Should create a not existing item', async function () {
      await instance.createItem('Item', 1);
      const storage = await instance.storage();
      const item = await storage.get('Item');
      assert.strictEqual(item.toNumber(), 1);
    });

    // Success test #3 - changeAmount with valid value

    it('Should change amount of an existing item', async function () {
      await instance.changeAmount('Item', 3);
      const storage = await instance.storage();
      const item = await storage.get('Item');
      assert.strictEqual(item.toNumber(), 3);
    });
  });
});
