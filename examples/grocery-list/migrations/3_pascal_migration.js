const GroceryPascal = artifacts.require('GroceryPascal');
const { MichelsonMap } = require('@taquito/taquito');

module.exports = async (deployer, _network) => {
  const storage = MichelsonMap.fromLiteral({});
  deployer.deploy(GroceryPascal, storage);
};
