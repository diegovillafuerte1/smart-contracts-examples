# Auction Dapp

## A simple auction smart contract example

#### Source code:

- **Caml:** [AuctionCaml.mligo](https://gitlab.com/tezosisrael/smart-contracts-examples/-/blob/master/examples/auction/contracts/AuctionCaml.mligo)
- **Pascal:** []()
- **Reason:** []()

#### Contract addresses:

- **Caml:** [KT1By2sW3pcCPdS7UnMziZmYKzT19x1rqQc2](https://better-call.dev/florencenet/KT1By2sW3pcCPdS7UnMziZmYKzT19x1rqQc2/operations)
- **Pascal:** []()
- **Reason:** []()

#### Entry points:

1. `place_bid (record { amount : tez; ad : address; })`
2. `withdraw`
3. `config(record { start : bool; period_days : int; period_hours : int; starting_price : tez;})`

#### Storage:

```ocaml
record {
    current_bidder : address;
    end_time : timestamp;
    current_price : tez;
    owner : address;
    starting_price : tez;
}
```

#### Errors:

##### while in progress:

- **"Not defined"**: main entry-point gets the command to call an entry-point, but the entry-point doesn't exist yet.
- **"Not implemented"**: the entry-point is defined, but not functional yet.

##### For final contract:

###### place_bid:

- **"Can't bid on owned artifact"**: owner of the auction is trying to place a bid.
- **"Auction is over"**: auction's end time passed before action was made.
- **"Bid is too low"**: the offered bid is lower than or equal to the last bid made.
- **"a transaction contract is missing"**: no contract exists to transfer the former highest bid back to.

###### withdraw:

- **"Auction not over yet"**: A withdrawal request was made while bid is still active.
- **"You are not authorized to make this transaction"**: a transaction request was made by an unauthorized account.
- **"a transaction contract is missing"**: no contract exists to complete withdrawal.

###### config:

- **"You do not have an authorization to configure auction"**: A configuration trial was made by someone other than the auction owner.
- **"Set 'start' field to 'true' to start auction"**: 'start' option is false, and the auction isn't started.
- **"Set a period to start auction"**: Both 'period_hour' and 'period_days' are set to 0, resaulting in an auction period of 0,
- **"Set an initial price to start auction"**: There is no initial price for the artifact being auctioned.

#### Tests:

##### place_bid:

- [x] Should try to place a first bid by the auction's owner. expected to fail with a message "Cant bid on owned artifact".

- [x] Should try to place a bid that is lower than the first asking price. expected to fail with a message "Bid is too low".

- [x] Should try to place a legal bid. storage field `current_price` is expected to have the new bid's amount.

##### withdraw:

- [x] Should try to withdraw by an account different than the owner's account. expected to fail with a message "You are not authorized to make this transaction".

- [x] Should try to withdraw before auction's end time. expected to fail with a message "Auction not over yet".

- [x] Should try to withdraw by the owner's account after auction's end time. storage field `current_price` is expected to be 0Tez.

##### config:

- [x] Should try to start auction with both `period_hours` and `period_days` fields set as 0. expected to fail with a message "Set a period to start auction".

- [x] Should try to start auction with `start` field unchanged (set as false). expected to fail with a message "Set 'start' field to 'true' to start auction".

- [x] Should try to start auction with `starting_price` field set as 0tez. expected to fail with a message "Set an initial price to start auction".

- [x] Should try to start auction with `period_hours` field set as 1, `start` field set as false, `starting_price` field set as 1000000 (1tez). expected to get a storage with values

```
    current_price: 1 tez
    end_time: now + 1hr
    staring_price: 1 tez
```
