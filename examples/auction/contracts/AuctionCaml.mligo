type storage = {
    current_bidder : address option;
    end_time : timestamp;
    current_price : tez;
    owner : address;
    starting_price : tez;
}


type config_params = {
    start : bool;
    period_days : int;
    period_hours : int;
    starting_price : tez;
}


type action = 
    | Place_bid
    | Withdraw
    | Config of config_params


type returnType = operation list * storage


let get_period (period_days, period_hours: int * int) : int =
    let hours = period_hours + (period_days * 24) in
    hours * 3600


let fail (text : string) : returnType =
    ([] : operation list), (failwith(text) : storage)


let find_contract (ad : address) : unit contract = 
    match ((Tezos.get_contract_opt ad) : (unit contract) option) with
    | None -> (failwith("a transaction contract is missing") : unit contract)
    | Some c -> c


let place_bid (store : storage) : returnType =
    if (store.owner = Tezos.source) then 
        fail ("Can't bid on owned artifact")
    else if (Tezos.now > store.end_time) then 
        fail ("Auction is over")
    else if (Tezos.amount <= store.current_price) then 
        fail ("Bid is too low")
    else  
        match store.current_bidder with
        | Some bidder -> 
        let former_bidder_contr = find_contract (bidder) in
        let former_bid = store.current_price in
        ([Tezos.transaction unit former_bid former_bidder_contr] : operation list), {store with current_price = Tezos.amount; current_bidder = Some (Tezos.source)}
        | None -> ([] : operation list), {store with current_price = Tezos.amount; current_bidder = Some (Tezos.source)}
    

let withdraw (store : storage) : returnType =
    if (Tezos.now < store.end_time) then
        fail ("Auction not over yet")
    else if (store.owner <> Tezos.source) then 
        fail ("You are not authorized to make this transaction")
    else
        let owner_contr = find_contract (store.owner) in
        ([Tezos.transaction unit store.current_price owner_contr] : operation list), {store with current_price = 0tez}
    

let configure (p, store : config_params * storage) : returnType = 
    let period = (get_period (p.period_days, p.period_hours)) in
    if (store.owner <> Tezos.source) then
        fail ("You do not have an authorization to configure auction")
    else if (p.start = false) then
        fail ("Set 'start' field to 'true' to start auction")
    else if (period = 0) then
        fail ("Set a period to start auction")
    else if (p.starting_price = 0tez) then
        fail ("Set an initial price to start auction")
    else
        ([] : operation list), {store with end_time = Tezos.now + period; current_price = p.starting_price; starting_price = p.starting_price;}


let main (action, store : action * storage) : returnType =
    match action with
    | Place_bid p -> place_bid(store)
    | Withdraw -> withdraw(store)
    | Config p -> configure(p, store)