type storage = {
    name: string,
    age: nat
};

type action =
    | SetName (string)
    | SetAge (nat)
    | SetAll ((string, nat))

type return = (list(operation), storage);

let setName = ((name, store): (string, storage)) : return => 
    ([]: list(operation), {...store, name : name });
let setAge = ((age, store): (nat, storage)) : return =>
    ([]: list(operation), {...store, age : age});
let setAll = ((name, age, store): (string, nat, storage)) : return => 
    ([]: list(operation), { ...store, name: name, age: age});

let main = ((action, store): (action, storage)) : return =>
    switch (action) {
        | SetName (v) => setName((v, store))
        | SetAge (v) => setAge((v, store))
        | SetAll (v) => setAll((v[0], v[1], store))
    };
