type action is
    | SetName of string
    | SetAge of nat
    | SetAll of string * nat

type storage is record [
    name : string;
    age : nat
]

type return is list(operation) * storage

function setName(const name : string; const store : storage) : return is
    ((nil : list(operation)), (store with record [name = name]))

function setAge(const age : nat; const store : storage) : return is
    ((nil : list(operation)), (store with record [age = age]))

function setAll(const name : string; const age : nat; const store : storage) : return is
    ((nil : list(operation)), (store with record [name = name; age = age]))

function main(const action : action; const store : storage) : return is
    case action of
    | SetName(v) -> setName(v, store)
    | SetAge(v) -> setAge(v, store)
    | SetAll(v) -> setAll(v.0, v.1, store)
    end