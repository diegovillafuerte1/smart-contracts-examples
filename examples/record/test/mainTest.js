const assert = require('assert');

//uncomment lang when proper contract exists / leave langs that you don't want to test commented
['Reason', 'Caml', 'Pascal'].forEach((lang) => {
  const contractName = 'Applicant' + lang;
  const Applicant = artifacts.require(contractName);
  contract(contractName, async function () {
    let instance = null;

    before(async () => {
      instance = await Applicant.deployed();
    });

    //First test - check if the storage was deployed correctly
    it('Initial storage should be empty', async function () {
      const storage = await instance.storage();
      assert.strictEqual(storage.name, '');
      assert.strictEqual(storage.age.toNumber(), 0);
    });

    //Second test - check the setName function
    it('SetName should change the name', async function () {
      const storageBefore = await instance.storage();
      const nameBefore = storageBefore.name;
      const newName = nameBefore + ' AddedName';
      await instance.setName(newName);
      const storage = await instance.storage();
      assert.strictEqual(storage.name, newName);
    });

    // Third test - check the setAge function
    it('SetAge should change the age', async function () {
      const storageBefore = await instance.storage();
      const ageBefore = storageBefore.age.toNumber();
      const newAge = ageBefore + 1;
      await instance.setAge(newAge);
      const storage = await instance.storage();
      assert.strictEqual(storage.age.toNumber(), newAge);
    });

    //Fourth  test - check the setAll function
    it('setAll should change name and age', async function () {
      const storageBefore = await instance.storage();
      const nameBefore = storageBefore.name;
      const ageBefore = storageBefore.age.toNumber();
      const newName = nameBefore + ' AddedName';
      const newAge = ageBefore + 1;
      await instance.setAll(newName, newAge);
      const storage = await instance.storage();
      assert.strictEqual(storage.name, newName);
      assert.strictEqual(storage.age.toNumber(), newAge);
    });
  });
});
