# Smart Contract Ideas for Repo

## Below is a list of ideas for smart contracts that will be written in LIGO to be added to a repo in order for new developers to learn from.

### Ideas to make:

- [x] Edit and update the storage of a record
- [x] Input a value to the storage of a map
- [ ] Calculator Dapp where the contract takes in int’s and does calculations
- [ ] Shopping list contract where you can make a list of “groceries” and you can remove items from the list by typing in the name or by clicking a button or something
- [ ] Contract that holds a set and you can add or remove
- [ ] A contract that implements an iterated operation which is conditional lists and removes any inputs that don’t fit the condition
- [ ] A contract with a map and within the map is records and the records can contain a profile of the users
- [ ] Head and tail contract with pattern matching to switches flow based on a value, so if the coin is head, print the words, Heads was flipped, if tails, print tails, In this contract, could also learn how to randomize in Ligo
- [ ] Import full files into a map or how to use metadata in a contract

### Creating new examples

This is the initial MR, it creates a bootstrapped project based on truffle, and makes it possible for other devs to work on different languages for this example.

replace $ISSUE_NUM with the issue number from gitlab and $SHORT_NAME with a short descriptive name for your example

```
git checkout -b example$ISSUE_NUM-init-$SHORT_NAME
cd examples
truffle init $SHORT_NAME
cd $SHORT_NAME
rm ./package-lock.json
yarn
```

Once this MR is merged, create a new branch (from master) named `example$ISSUE_NUM-$SHORT_NAME-$LANG`

you can add your contract to `$SHORT_NAME/contracts`

### Creating dApps

Every dApp is a page inside the app the is built from `web/`. We need to add the dApp itself to a folder under `web/components`, a reference to it inside `web/components/examples.js`, and the example data inside `data/examples.json`

To run the webapp run: `yarn web:dev` from the root folder or `yarn dev` from `web` folder

### Branch name format

`example#-LANGUAGE-short-name`

where:

- \# is the number of the issue
- LANGUAGE is one of `michelson`, `caml`, `reason`, `pascal`, `archetype`, `smartpy`

### Test Instructions

you can find the test instructions in TESTINSTRUCTIONS.md file
